﻿using DemoMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using DemoMVC.Filter;

namespace DemoMVC.Controllers
{
    [Authorize]
    [CustomFilter]
    [HandleError(ExceptionType = typeof(Exception), View = "Error")]
    public class BaseController : Controller
    {
        // GET: Base
    protected SunbeamDBEntities dBobj  { get; set; }
    public BaseController()
    {
        this.dBobj = new SunbeamDBEntities();
    }
}
}