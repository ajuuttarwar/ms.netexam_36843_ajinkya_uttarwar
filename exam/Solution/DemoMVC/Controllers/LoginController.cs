﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DemoMVC.Controllers;
using DemoMVC.Models;

namespace DemoMVC.Controllers
{
    public class LoginController : Controller
    {
        SunbeamDBEntities dBobj = new SunbeamDBEntities();
        // GET: Login
        public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(HRLoginInfo loginobject, string ReturnUrl)
        {
            var MatchCount = (from hr in dBobj.HRLoginInfoes.ToList()
                              where hr.UserName.ToLower() == loginobject.UserName.ToLower()
                              && hr.Password == loginobject.Password
                              select hr).ToList().Count();
            if (MatchCount == 1)
            {
                FormsAuthentication.SetAuthCookie(loginobject.UserName, false);
                if (ReturnUrl != null)
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return Redirect("/Home/Index");
                }
            }
            else
            {
                ViewBag.ErrorMessage = "User Name and Password is Incorrect";
                return View();
            }
        }
        public ActionResult Signout()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Home/Index");
        }
    }
}