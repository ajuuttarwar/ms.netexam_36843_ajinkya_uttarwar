﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DemoMVC.Filter;
using DemoMVC.Helper;

namespace DemoMVC.Filter
{
    public class CustomFilter: ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Logger.CurrentLogger.Log(string.Format("/{0}/{1} is to execute",
                filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                filterContext.ActionDescriptor.ActionName));
        }
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            Logger.CurrentLogger.Log(string.Format("/{0}/{1} executed!!",
               filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
               filterContext.ActionDescriptor.ActionName));
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            Logger.CurrentLogger.Log("UI Begin");
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            Logger.CurrentLogger.Log("UI Completed");
        }
    }
}